import React from 'react' 
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Schools from './views/School/Schools';
import ViewSchoolsDetails from './views/School/ViewSchoolsDetails';
import LogIn from './views/Login/Login';

import './scss/comman.scss';
import './scss/SchoolsList.scss';
import './scss/loginPage.scss';
import './scss/SchoolDetails.scss';
import './scss/headerAddNew.scss';

import {store} from './redux/Store';
import {Provider} from "react-redux";

function App() {
  return ( 
    <>
    <Provider store={store}> 
      <Router> 
        <Switch> 
          <Route exact path="/" component={LogIn} /> 
          <Route exact path="/schools" component={Schools} /> 
          <Route exact path="/view-school-details/:id" component={ViewSchoolsDetails} /> 
        </Switch>
      </Router>
    </Provider>
    </> 
  );
}

export default App;
