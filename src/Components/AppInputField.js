import React from 'react'

function AppInputField(props) {
    return (
        <div style={{textAlign: 'left'}}>
            <lable>{props.lable}</lable>
            <input type={props.type} 
                    autoComplete="off"
                    onChange={props.onChange}
                    name={props.name || ''}
                    placeholder={props.placeholder || ''} 
                    className={props.className + ' appInputs'}
                    value={props.value} />
        </div>
    )
}

export default AppInputField
