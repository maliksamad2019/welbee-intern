import Reducer from './Reducer';
import {createStore} from 'redux';
import {getAllSchools as getAllSchoolsURL} from '../APIs/ApiUrls';
import axios from 'axios';
import {saveSchooolList} from './ActionCreater';

export const store = createStore(Reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

(async () => {
    const token = localStorage.getItem('token');
    
    axios.get(getAllSchoolsURL, {headers: {"Authorization":`Bearer ${token}`} } )
    .then(response => {  
            store.dispatch(saveSchooolList(response.data.data)); 
        }
    ); 
})();