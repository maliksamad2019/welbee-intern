 import {SAVE_SCHOOL_LIST} from './ActionTypes';


const initialState =  {schools: []}


export const Reducer = (state = initialState, action) => {
    
    switch(action.type){
        case SAVE_SCHOOL_LIST:
            return {...state, schools: action.schools}; 
        
        default:
            return {...state};
    }
}

export default Reducer;

