import {SAVE_SCHOOL_LIST} from './ActionTypes';

export const saveSchooolList = (list) => {
    return {
        type: SAVE_SCHOOL_LIST,
        schools: list 
    }
}

export const filterSchooolList = (list) => {
    return {
        type: SAVE_SCHOOL_LIST,
        filtered: list 
    }
}