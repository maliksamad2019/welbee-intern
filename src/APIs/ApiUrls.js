const BASE_URL = 'http://api.d.welbee.co.uk/api/';

export const getAllSchools = `${BASE_URL}School/GetAllSchools?schoolListingFlag=`;

export const getSchoolByID = `${BASE_URL}School/`;

export const loginAPI = `${BASE_URL}Account/Login`;
