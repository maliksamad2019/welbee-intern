import React from 'react'
import Header from '../Header/Header';

export const withHeader = (WrappedComponent) => {
    
    return (props) => {
        
        // this HOC will check if token exist in local storage
        // and if exist check it if it is valid 
        // onInvalid Token don't render WrappedComponent
         
        return (
            <>
                <Header />
                <WrappedComponent {...props} />
            </>
        ); 
    };
}