import React, { useState } from 'react'
import axios from 'axios';

import {loginAPI as loginURL} from '../../APIs/ApiUrls';
import {AppInputField} from '../../Components'
import {withHeader} from '../hoc/HOC';


function Login(props) {
    let [formData, setFormData] = useState({ Email: '', Password: '' });
    let [response, setResponse] = useState('');

    const  handleChange = (e) => {
        let _formData = formData;
        _formData[e.target.name] = e.target.value;
        console.log(_formData);
        
        setFormData({..._formData});
    }

    const handleSubmit = (e) => {
        e.preventDefault(); 

        axios.post(loginURL, formData, { headers: { 'Content-Type': 'application/json' }     })
        .then(response => 
            {
                setResponse(response.data); 
                
                if(response.data.data) {
                    localStorage.setItem('token', response.data.data.access_token); 
                    props.history.push('/schools');  
                }
            } 
        ); 
    }

    return (
        <div className="alignCenter">
                <div className="lightBorder loginForm">
                    <h1>Log In</h1>
                    <span style={{color:"red"}}>{response.message}</span>
                    
                    <form onSubmit={handleSubmit}>
                        <AppInputField type="text" 
                                        placeholder="Email" 
                                        name="Email"
                                        onChange={handleChange} /> 
                        <AppInputField type="password" 
                                        name="Password"
                                        placeholder="password" 
                                        onChange={handleChange} />


                        <div style={{textAlign:'right'}}>
                            <button type='submit' className="styledBtn">Login</button>
                        </div>
                    </form>
                </div>
            </div>
    )
}

export default withHeader( Login );
