import React, { useState, useEffect } from 'react'
import SingleSchoolRow from './SingleSchoolRow';

import {saveSchooolList} from '../../redux/ActionCreater';
import {connect} from 'react-redux';
import {withHeader} from '../hoc/HOC';
 

function Schools(props) { 

    let [SchoolsList, setSchoolsList] = useState(props.schools)
    let [FilteredSchoolsList, setFilteredSchoolsList] = useState([]) 
    
    useEffect( () => {
            setSchoolsList(props.schools)
            setFilteredSchoolsList([...props.schools]);
        }
    , [props.schools]);

    const handleSearch = (e) => { 
        const query = e.target.value.toLowerCase() 
        const filtered = SchoolsList.filter(item => (item.Name.toLowerCase().includes(query) 
                                                    || item.Address.toLowerCase().includes(query) ) )

        setFilteredSchoolsList([...filtered]);
    }

    const deleteSingleItem = (id) => { 
        if(!global.confirm(`Are you Sure, to delete this school of ID: ${id}`))
            return;

        let schools = SchoolsList; 
        schools = schools.filter(item => item.Id !== id)
            
        let filteredschools = FilteredSchoolsList; 
        filteredschools = filteredschools.filter(item => item.Id !== id)
        
        setFilteredSchoolsList([...filteredschools]);
        //setSchoolsList(schools);
        
        props.saveAllSchoolsToRedux([...schools]);
    }
 
    
    return (
        <>
            <div style={{textAlign: "center"}}>
                <h2 style={{color:"#6e6e6e"}}>Schools Listing [By Internee]</h2>
                <div className="searchForm">
                    <form>
                        <input type="text" onChange={handleSearch} placeholder="Search..."/> 
                    </form>
                </div> 
                <table className="user-list"> 
                    <tbody> 
                        <tr>
                            <td><input hidden 
                                        className="scale" type="checkbox" /></td>
                            <td><span>Image</span></td>
                            <td><span>Name</span></td>
                            <td><span>NumberOfPupil</span></td>
                            <td><span>Address</span></td>
                            <td><span>ContactNumber</span></td>
                            <td><span>Actions</span></td>
                        </tr>
                        {
                            FilteredSchoolsList.map(item => (
                                <SingleSchoolRow  key={item.Id} 
                                                    item={item} 
                                                    deleteSingleItem={deleteSingleItem}/>
                                ))
                        }
                        
                    </tbody>
                </table>
            </div>
        </>
    )
}


const mapStateToProps = (state) =>{ 
    return {...state }
}

const mapDispatchToProps = (dispatch) => { 
    return {
        saveAllSchoolsToRedux: (list) => dispatch(saveSchooolList(list)) 
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(    withHeader(Schools)  );
