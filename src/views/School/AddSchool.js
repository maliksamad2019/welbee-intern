import React, { useState } from 'react'
import {connect} from 'react-redux'

import AppInputField from '../../Components/AppInputField';
import {toggleClassByID} from '../../HelperFunctions/ForStyling';
import {saveSchooolList} from '../../redux/ActionCreater';

const initialFormData = {
    AddedDate: "",
    Address: "",
    AdminName: null,
    Base64Image: null,
    ContactNumber: "",
    Email: null,
    File: null,
    FileName: null,
    FirstSurveySubmissionDate: null,
    HeadTeachers: [],
    HearAboutUs: "",
    Id: 1,
    ImageName: null,
    InvoiceAddressEmail: "",
    InvoiceAddressName: "",
    IsArchived: false,
    IsStaffExist: false,
    IsWelbeeVoice: true,
    Logo: null,
    LogoId: null,
    Name: "",
    NumberOfPupil: 0,
    NumberOfStaff: 0,
    Password: null,
    PhaseOfSchool: 0,
    Role: null,
    Sector: 1,
    SignUpStatus: 3,
    SurveyStartDate: "",
    UpdatedDate: "",
    UserId: null,
 } 

export function AddSchool(props){
  
    let [formData, setFormData] = useState({...initialFormData});

    const handleFieldChange = (e) => {
        let _formData = formData;
        _formData[e.target.name] = e.target.value;

        setFormData({..._formData});
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();

        const NewID = (new Date()).getTime();
        let allSchools = props.schools;
        let _formData = formData;

        _formData.Id = NewID;
        allSchools.push(_formData);
        props.saveAllSchoolsToRedux([...allSchools]);
        closePopUp();

        alert('Successfully Added New School to Redux');
    }

    const closePopUp = () => {
        toggleClassByID('addNewPopUp', 'show');
        setFormData({...initialFormData});
    }
     

    return ( 
        <div id='addNewPopUp' className="popup">
            <div>
                <i class="material-icons cross" 
                    onClick={() => toggleClassByID('addNewPopUp', 'show')}>close</i>

                <h1>Add New</h1>
                <form onSubmit={handleFormSubmit}>
                    <AppInputField type='text' 
                                    lable='Name' 
                                    placeholder="Name"
                                    name='Name' 
                                    onChange={handleFieldChange}/>
                                    
                    <AppInputField type='text' 
                                    lable='Address' 
                                    placeholder="Address"
                                    name='Address' 
                                    onChange={handleFieldChange}/>
                                    
                    <AppInputField type='number' 
                                    lable='Contact Number' 
                                    placeholder="Contact Number"
                                    name='ContactNumber' 
                                    onChange={handleFieldChange}/>
                                    
                    <AppInputField type='number' 
                                    lable='Number Of Pupil' 
                                    placeholder="Number Of Pupil"
                                    name='NumberOfPupil' 
                                    onChange={handleFieldChange}/>
                                    
                    <AppInputField type='number' 
                                    lable='Number Of Staff' 
                                    placeholder="Number Of Staff"
                                    name='NumberOfStaff' 
                                    onChange={handleFieldChange}/>
                                    
                    <AppInputField type='email' 
                                    lable='Email' 
                                    placeholder="Email"
                                    name='Email' 
                                    onChange={handleFieldChange}/>
                                    
                    <AppInputField type='password' 
                                    lable='Password' 
                                    placeholder="Password"
                                    name='Password' 
                                    onChange={handleFieldChange}/>

                    <div className="buttonDiv">
                        <button type='reset' onClick={closePopUp}>Cancel</button>
                        <button type='submit'>Add</button>
                    </div>

                </form>
            </div>
        </div>
    ) 
}


const mapStateToProps = (state) =>{ 
    return {...state }
}

const mapDispatchToProps = (dispatch) => { 
    return {
        saveAllSchoolsToRedux: (list) => dispatch(saveSchooolList(list))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddSchool);
