import React from 'react'
import {Link} from 'react-router-dom';

function SingleSchoolRow(props) {

    const defaultImage = "https://www.freeiconspng.com/uploads/no-image-icon-21.png";

    return (
        <tr > 
            <td><input 
                        className="scale" 
                        type="checkbox" /></td>
            <td> <div   style={{backgroundImage:`url(${props.item.Base64Image ? `data:image/jpeg;base64,${props.item.Base64Image}` : defaultImage})`}} 
                    className="profile-image"></div> 
            </td>
            <td><span>{props.item.Name}</span></td>
            <td><span>{props.item.NumberOfPupil}</span></td>
            <td><span>{props.item.Address}</span></td>
            <td><span>{props.item.ContactNumber}</span></td>
            <td>
                <span>
                    <Link to={`/view-school-details/${props.item.Id}`}>
                         <span className="material-icons edit" title="view">description</span>
                    </Link>
                    <span className="material-icons delete" 
                            onClick={() => props.deleteSingleItem(props.item.Id)} 
                            title="Delete">&#xE872;</span>
                </span>
            </td>
        </tr>
    )
}

export default SingleSchoolRow
