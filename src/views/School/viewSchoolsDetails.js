import React, {  useEffect } from 'react' 
import { AppInputField  } from '../../Components';
 

import {saveSchooolList} from '../../redux/ActionCreater';
import {connect } from 'react-redux';
import useSchoolDetials from '../CustomHooks/useSchoolDetails';
import {withHeader} from '../hoc/HOC';

function ViewSchoolsDetails (props){
  
    let {details, handleChangesInDetials, initialized} = useSchoolDetials(props);
    
    useEffect(()=>{  
        initialized();

        // eslint-disable-next-line
    }, [props.schools]);
    
    // useEffect( () => {setDetails(props.schools);} );

    const handleFieldChange = (e) => handleChangesInDetials(e.target)

    const handleFormSubmit = (e) => { 
        e.preventDefault();
        const schools = props.schools.map( item => {
            if(item.Id === details.Id)
                return details;
            else
                return item;
        });
 
        props.saveAllSchoolsToRedux([...schools]);
        alert('Successfully Updated!'); 
        props.history.push('/schools'); 
    } 
 
    if(!details)
        return 'Loading...';

    return (
        <div className="schoolDetails">
            <div>
                <form onSubmit={handleFormSubmit}>
                    <h1>School Details with ID: {props.match.params.id}</h1>
                    <AppInputField name="Name"  
                                    placeholder="School Name" 
                                    onChange = {handleFieldChange}
                                    value={details.Name  || ''} />
                    <br />

                    <AppInputField name="AdminName"  
                                    placeholder="Admin name"
                                    onChange = {handleFieldChange}
                                    value={details.AdminName  || ''}  />
                    <br />

                    <AppInputField name="Email"  
                                    placeholder="Email"
                                    onChange = {handleFieldChange}
                                    value={details.Email  || ''}
                                    type="email"  />
                    <br />
                    
                    <AppInputField name="ContactNumber"  
                                    placeholder="ContactNumber"
                                    onChange = {handleFieldChange}
                                    value={details.ContactNumber  || ''}
                                    type="number"  />
                    <br />
                                    
                    <AppInputField name="Address"  
                                    placeholder="Address"  
                                    onChange = {handleFieldChange}
                                    value={details.Address  || ''}/>
                    <br />
                                    
                    <AppInputField name="Password"
                                    placeholder="Password"
                                    type="password"  
                                    onChange = {handleFieldChange}
                                    value={details.Password || '' }/>
                    <br />
                    
                    <AppInputField name="NumberOfStaff"
                                    placeholder="No of Staff"
                                    type="number"  
                                    onChange = {handleFieldChange}
                                    value={details.NumberOfStaff  || ''}/>
                    <br />
                                    
                    <AppInputField name="NumberOfPupil"
                                    placeholder="No of Pupils" 
                                    type="number"  
                                    onChange = {handleFieldChange} 
                                    value={details.NumberOfPupil  || ''}/>
                    <br />

                    <input type="submit" className="appInputs" value="Update"/>
                    
                </form>
            </div>
        </div>
    ) 
}


const mapStateToProps = (state) =>{ 
    return {...state }
}

const mapDispatchToProps = (dispatch) => { 
    return {
        saveAllSchoolsToRedux: (list) => dispatch(saveSchooolList(list))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(    withHeader(ViewSchoolsDetails)  );
