// eslint-disable-next-line
import React, { useState } from 'react' 

export default function useSchoolDetials(props){

    const SchoolDetail = props.schools.find(item => item.Id === parseInt(props.match.params.id))
    
    const initialized = () => {
        // if (props.schools !== undefined && props.schools.length > 0 && details === undefined )
            setDetails(() => {  
                const details = props.schools.find(item => item.Id === parseInt(props.match.params.id)); 
                return { ...details};
            });
    }

    let [details, setDetails] = useState( SchoolDetail );

    const handleChangesInDetials = (targetField) => {
        const _details = details;
        const name = targetField.name;
        const value = targetField.value;
        
        _details[name] = value;  
        
        setDetails({..._details});
    }

    return {details, handleChangesInDetials, initialized};
}
