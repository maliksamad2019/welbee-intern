import React from 'react'
import {Link} from 'react-router-dom';
import AddSchool from '../School/AddSchool';

import {toggleClassByID} from '../../HelperFunctions/ForStyling';

function Header() {
 
    return (
        <> 
            <AddSchool />
            <div className="header">
                <Link to="/"> <button>Login</button> </Link>
                <Link to="/schools"> <button>Schools</button> </Link>
                <button onClick={() => toggleClassByID('addNewPopUp', 'show')}>Add New</button> 
            </div>
        </>
    )
}

export default Header
