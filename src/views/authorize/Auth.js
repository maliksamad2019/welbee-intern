import React from 'react'

function withSubscription(WrappedComponent) {
    
    return class extends React.Component {
        


        // this HOC will check if token exist in local storage
        // and if exist check it if it is valid 
        // onInvalid Token don't render WrappedComponent


        render() {  
            return <WrappedComponent {...this.props} />;
        }
    };
}
